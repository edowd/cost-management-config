#!/bin/bash

s3cmd sync \
--acl-public \
--exclude "upload.sh" \
--exclude ".git" \
--exclude ".git*" \
--default-mime-type="text/plain" \
. \
s3://public.dowdandassociates.com/products/cloud-billing/cost-management-config/

